# Production Architecture

## Diagram
```mermaid
graph LR
    A[codechallenge.dev] --> B
    C[docs.codechallenge.dev] --> B
    B -- codechallenge.dev --> APP

    subgraph GCP
    B(Cloud DNS)
    RUN(Cloud Run)
    APP(App Engine Node.js app)
    RUN -- 5 minutes cron --> APP
    OA(OAuth)
    APP --> OA
    SQL(Cloud SQL)
    APP <--> SQL
    end

    B -- docs. --> P
    OA <--> O
    APP <-- Check for completion --> API

    subgraph GitLab
    P(GitLab Pages)
    API(GitLab API)
    O(GitLab Sign In)
    end
```

## Services Used

### App Engine

We use [Google App Engine](https://cloud.google.com/appengine/docs) to deploy the Node JS app into the `group-community-a29572` GCP project. This requires a few things – the starting point is the `app.yaml` file that is checked into this repository. This contains all the “safe” (non-secret) information about how we deploy and scale the app.

For environmental variables that are secret (such as the Postgres password, OAuth credentials, etc.), we include a `app_env.yaml` file that isn't checked in to git. This file is stored as a base64 encoded variable in the GitLab CI/CD variables for the project. More details on how to download, edit, and re-upload this variable are found in the README.md. The secrets used in those variables can be found in 1Password in the secure note about the Code Challenge in the Marketing vault.

### GitLab OpenID connect for sign in

For sign in, we use gitlab.com and a GitLab [OAuth application](https://docs.gitlab.com/ee/integration/oauth_provider.html) to enable single-sign in only using OpenID Connect. This application is called “GitLab Code Challenge" and is added to GitLab.com under the "gitlab_dev_evangelism_bot" user (available in the Marketing 1Password account).

This application is marked as "confidential" so that it receives a client secret, and is scoped to have these permissions: openid, profile, email. The valid callback URLs are:

```
http://localhost:3000/callback
https://codechallenge.dev/callback
```

The Node JS app uses the environmental variables `ISSUER_BASE_URL`, `CLIENT_ID`, and `CLIENT_SECRET` to connect to GitLab.com. It then sends users through their GitLab.com sign in process to authorize them to the Code Challenge (and give us their GitLab username and email for tracking purposes). These values are available in the Marketing 1Password secure note about the Code Challenge.

### GitLab API

We use the GitLab API to verify completion of challenges. This API key is added under the "gitlab_dev_evangelism_bot" user, is called `code-challenge-dot-dev` and has the scopes: `api`, `read_api`, `read_user`, and `read_repository`. This variable is part of the environmental variables stored in the `app_env.yaml` described above.

### Cloud SQL

For the database, we use [GCP Cloud SQL](https://cloud.google.com/sql/docs/postgres). The instance is named `codechallenge-prod` and the password can be found in the 1Password secure note about the Code Challenge in the Marketing vault.

The Node JS app knows how to connect to this database using the environmental variables `INSTANCE_CONNECTION_NAME`, `PGHOST`, `PGUSER`, `PGPASSWORD`, and `PGDATABASE`. Those variables are part of the environmental variables stored in the `app_env.yaml` described above.

To connect to the database from your local machine, you can run: `gcloud sql connect codechallenge-prod --user=postgres` and then provide the password. That will temporarily allow your local IP to access the database and drop you into a `psql` session (though you can close that session and access the database, run migrations, etc from other places)

### Cloud Storage

One public [cloud storage](https://cloud.google.com/storage/docs) bucket is used for any uploaded images. The bucket name is `codechallenge-public`

### Cloud Run Cron job

In the `cron.yaml` file, we define one [Cloud Run](https://cloud.google.com/run/docs) function to run every five minutes. That job just calls the URL `/tasks/checkScores`. When that URL is hit, we run check the scores for all active challenges. You can also call this URL manually yourself or by using the "Check Scores" button in any challenge.

The timing of this can be adjusted to be shorter if desired for a large show. You'll just need to redeploy the app for it to take effect.

### Cloud DNS

Using [Cloud DNS](https://cloud.google.com/dns) and the App Engine setting for custom domains, we serve the App Engine endpoint to `codechallenge.dev`. The current records for this are:

```
A   216.239.32.21
A   216.239.34.21
A   216.239.36.21
A   216.239.38.21
AAAA    2001:4860:4802:32::15
AAAA    2001:4860:4802:34::15
AAAA    2001:4860:4802:36::15
AAAA    2001:4860:4802:38::15
CNAME   ghs.googlehosted.com    www
```

## CI/CD and Production deployments

We deploy automatically for all new tags using GitLab CI/CD.  The basic steps of the production deploy are:

- Transform the secret variables into files. 
    - As described above, we store secret environmental variables for things like OAuth, Postgres, etc. in a CI variable called ``. This is a base64 encoded string of a yaml file. For more information on this, see the README.md. - We also store (as a base64 encoded string) the JSON file for the service user used to do the deployment in `GCP_SERVICE_KEY`.  See below for details on that service user.
- Once we have those two files, we set the project ID using the `GCP_PROJECT_ID` CI variable.
- Then we run `gcloud app deploy --quiet` which uses the app.yaml (and now created `app_env.yaml` to deploy the code from the project to App Engine
- Finally we run `gcloud app deploy cron.yaml --quiet` to deploy the cron functions as described in the Cloud Run section above

### Service User

To deploy, we use a IAM service account named `code-challenge-deploy@group-community-a29572.iam.gserviceaccount.com`. This user has the following permissions on GPC:

- App Engine Admin
- App Engine Service Admin
- Cloud Build Service Account
- Cloud Scheduler Admin
- Compute Storage Admin

The credentials for this account are stored in a base64 CI/CD variable named `GCP_SERVICE_KEY` which is decoded in the a JSON file in the production deploy portion of the .gitlab-ci.yml and then used to authorize gcloud to deploy.

### Documentation

The documentation is deployed to GitLab Pages (see the `pages:` section of the .gitlab-ci.yml) which is then routed with DNS to `docs.codechallenge.dev`.
