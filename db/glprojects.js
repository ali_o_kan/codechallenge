const { sql } = require("@databases/pg");
const db = require("./connection");

async function getByGitLabID(id) {
  return db.query(sql`
        SELECT *
        FROM glprojects
        WHERE id = ${id}
    `);
}

//name_with_namespace
async function getByNamespace(name_with_namespace) {
  return db.query(sql`
        SELECT *
        FROM glprojects
        WHERE name_with_namespace = ${name_with_namespace}
    `);
}

async function upsert(glprjoject) {
  const { id, name, name_with_namespace, path, path_with_namespace, default_branch, http_url_to_repo, web_url, avatar_url, forks_count, star_count, visibility, description, namespace } = glprjoject;
  let groupId = 0;
  if (namespace.kind === 'group') groupId = namespace.id;
  const out = await db.query(sql`
    INSERT INTO glprojects (id, name, name_with_namespace, path, path_with_namespace, default_branch, http_url_to_repo, web_url, avatar_url, forks_count, star_count, visibility, description, group_id)
    VALUES(${id}, ${name}, ${name_with_namespace}, ${path}, ${path_with_namespace}, ${default_branch}, ${http_url_to_repo}, ${web_url}, ${avatar_url}, ${forks_count}, ${star_count}, ${visibility}, ${description}, ${groupId}) 
    ON CONFLICT (id) 
    DO 
      UPDATE SET id = ${id}, 
      name = ${name}, 
      name_with_namespace = ${name_with_namespace}, 
      path = ${path}, 
      path_with_namespace = ${path_with_namespace}, 
      default_branch = ${default_branch}, 
      http_url_to_repo = ${http_url_to_repo}, 
      web_url = ${web_url}, 
      avatar_url = ${avatar_url}, 
      forks_count = ${forks_count}, 
      star_count = ${star_count}, 
      visibility = ${visibility}, 
      description = ${description},
      group_id = ${groupId}
  `)

  return out;
}

module.exports = { getByGitLabID, getByNamespace, upsert };
