const info = require("./info");
const users = require("./users");
const challenges = require("./challenges");
const glprojects = require("./glprojects");
const levels = require("./levels");
const users_challenges = require("./users_challenges");
const scoring = require("./scoring");
const challenge_webhooks = require("./challenge_webhooks");
const user_badges = require("./user_badges");
const signups = require("./signups");

module.exports = {
  info,
  users,
  challenges,
  glprojects,
  levels,
  users_challenges,
  scoring,
  challenge_webhooks,
  user_badges,
  signups,
};
