CREATE TABLE users (
  id            serial PRIMARY KEY,
  name          varchar(200) NOT NULL,
  gitlab_handle varchar(200) NOT NULL,
  gitlab_id     INTEGER NOT NULL,
  email         TEXT,
  picture       TEXT,
  last_login    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  created_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at    TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON users
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();