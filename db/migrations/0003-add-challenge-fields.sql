ALTER TABLE challenges
ADD COLUMN end_date DATE,
ADD COLUMN created_by INT,
ADD CONSTRAINT fk_users
    FOREIGN KEY(created_by)
    REFERENCES users(id); 
