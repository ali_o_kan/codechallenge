CREATE TABLE glprojects (
  id            INTEGER PRIMARY KEY,
  name          varchar(100) NOT NULL,
  name_with_namespace TEXT,
  path          varchar(100) NOT NULL,
  path_with_namespace TEXT,
  default_branch varchar(100),
  http_url_to_repo TEXT,
  web_url       TEXT,
  avatar_url    TEXT,
  forks_count   INTEGER,
  star_count    INTEGER,
  visibility    varchar(40),
  description   TEXT,
  created_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at    TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON glprojects
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();