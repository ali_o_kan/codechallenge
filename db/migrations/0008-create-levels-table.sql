CREATE TABLE levels (
  id            serial PRIMARY KEY,
  challenge_id  INTEGER NOT NULL,
  level         INTEGER NOT NULL DEFAULT 1,
  title         varchar(250) NOT NULL,
  description   TEXT,
  type          varchar(40) NOT NULL,
  details       JSON,
  created_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at    TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

ALTER TABLE levels
ADD CONSTRAINT fk_challenges
    FOREIGN KEY(challenge_id)
    REFERENCES challenges(id); 

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON levels
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();