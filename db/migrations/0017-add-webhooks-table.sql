CREATE TABLE challenge_webhooks (
  challenge_id  INT PRIMARY KEY,
  hook_url      TEXT,
  newEntrant    BOOLEAN,
  levelComplete BOOLEAN,
  created_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at    TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

ALTER TABLE challenge_webhooks 
ADD CONSTRAINT fk_challenges
    FOREIGN KEY(challenge_id)
    REFERENCES challenges(id); 

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON challenge_webhooks
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();