CREATE TABLE user_badges (
  id            serial PRIMARY KEY,
  level_id      INTEGER NOT NULL,
  challenge_id  INTEGER NOT NULL,
  user_id       INTEGER NOT NULL,
  badge         TEXT,
  ref           TEXT,
  challenge_name varchar(40),
  level_title   varchar(250),
  level         INT,
  created_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at    TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

ALTER TABLE user_badges
ADD CONSTRAINT fk_challenges
    FOREIGN KEY(challenge_id)
    REFERENCES challenges(id),
ADD CONSTRAINT fk_levels
    FOREIGN KEY(level_id)
    REFERENCES levels(id),
ADD CONSTRAINT fk_users
    FOREIGN KEY(user_id)
    REFERENCES users(id);

ALTER TABLE user_badges
ADD CONSTRAINT unique_badge
UNIQUE (level_id, challenge_id, user_id);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON user_badges
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();