CREATE TABLE "deleted_user_badges" AS TABLE "user_badges" WITH NO DATA;

CREATE FUNCTION moveDeleteduser_badges() RETURNS trigger AS $$
  BEGIN
    INSERT INTO "deleted_user_badges" VALUES((OLD).*);
    RETURN OLD;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER moveDeleteduser_badges
BEFORE DELETE ON "user_badges"
FOR EACH ROW
EXECUTE PROCEDURE moveDeleteduser_badges();