CREATE TABLE signups (
  id                serial PRIMARY KEY,
  challenge_id      INTEGER NOT NULL,
  user_id           INTEGER NOT NULL,
  lastname_fi       varchar(250),
  agree_to_rules    BOOLEAN,
  levels_completed  BOOLEAN DEFAULT FALSE,
  created_at    TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at    TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

ALTER TABLE signups
ADD CONSTRAINT fk_challenges
    FOREIGN KEY(challenge_id)
    REFERENCES challenges(id),
ADD CONSTRAINT fk_users
    FOREIGN KEY(user_id)
    REFERENCES users(id);

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON signups
  FOR EACH ROW
  EXECUTE PROCEDURE trigger_set_timestamp();