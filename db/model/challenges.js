/**
 * @typedef {Object} Challenge
 * @property {number} id Challenge ID
 * @property {String} name Challenge name
 * @property {String} description Challenge description
 * @property {Date} start_date Start date of the challenge
 * @property {Boolean} active Whether or not the challenge is active
 * @property {String} project_name GitLab project name (e.g. gitlab-org/gitlab)
 * @property {Number} project_id GitLab Project ID
 * @property {Date} end_date End date of the challenge
 * @property {Number} created_by User ID of the creator
 * @property {String} shortname Short name of the challenge (used in short URLs)
 * @property {Boolean} featured Is this challenge featured on the homepage?
 * @property {String} rules The link to the terms & rules for this challenge
 * @property {Date} created_at Date/time the record was created
 * @property {Date} updated_at Date/time the record was updated
 */

module.exports = { Challenge };
