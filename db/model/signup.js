/**
 * @typedef {Object} SignUp
 * @property {number} id The ID for the signup
 * @property {number} challenge_id The id for the related challenge (challenge.id)
 * @property {number} user_id The id for the user who completed this challenge (users.id)
 * @property {string} lastname_fi Last name and first inital of the entrant
 * @property {boolean} agree_to_rules If the entrant agreed to the rules
 * @property {boolean} levels_completed Has the user ever completed any levels on this challenge?
 * @property {Date} created_at Date/time the record was created
 * @property {Date} updated_at Date/time the record was updated
 */

 module.exports = { SignUp };
