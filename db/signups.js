const { sql } = require("@databases/pg");
const PubSub = require("pubsub-js");

const db = require("./connection");

const isStandalone = process.env.STANDALONE;
const standaloneData = require("../data/challenges.json");
const { logger } = require("../lib/logger");
const { iso8601, formatAllDates } = require("../lib/time");

async function getAllByChallengeId(challengeId) {
  let signups = [];
  if (isStandalone) {
    /// TODO
  } else {
    signups = await db.query(sql`
        SELECT *
        FROM signups
        WHERE challenge_id = ${challengeId}
    `);
  }

  return signups;
}

/**
 *
 * @param {import("./model").signups.SignUp} signup The details of the signup
 * @returns
 */
async function create(signup) {
  const res = await db.query(sql`
    INSERT INTO signups (challenge_id, user_id, lastname_fi, agree_to_rules) 
    VALUES(${signup.challenge_id}, ${signup.user_id}, ${signup.lastname_fi}, ${signup.agree_to_rules})
    RETURNING id;
  `);
  const newSignupId = res[0].id;

  PubSub.publish("newChallenge", {
    newSignupId,
    source: "db.signups.create",
  });
  return res;
}

async function markCompleted(challengeId, userId) {
    const res = await db.query(sql`
        UPDATE signups
        SET levels_completed = TRUE
        WHERE challenge_id = ${challengeId} 
        AND user_id = ${userId}
    `)
    return res;
}

module.exports = {
  getAllByChallengeId,
  create,
  markCompleted,
};
