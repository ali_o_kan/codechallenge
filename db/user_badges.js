const { sql } = require("@databases/pg");
const PubSub = require("pubsub-js");

const db = require("./connection");

const isStandalone = process.env.STANDALONE;
const standaloneData = require("../data/challenges.json");
const { logger } = require("../lib/logger");

async function add(
  userId,
  challengeId,
  levelId,
  badge,
  ref,
  challenge_name,
  level_title,
  level
) {
  db.query(
    sql`
    INSERT INTO user_badges (level_id, challenge_id, user_id, ref, badge, challenge_name, level_title, level)
    VALUES (${levelId}, ${challengeId}, ${userId}, ${ref}, ${badge}, ${challenge_name}, ${level_title}, ${level})
  `
  )
    .then((res) => {
      PubSub.publish("badgeAwarded", {
        levelId,
        challengeId,
        userId,
        ref,
        badge,
      });
      return res;
    })
    .catch((e) => {
      logger.error("Error in db.user_badges.add", e);
    });
}

async function getByUserId(userId) {
  const res = await db.query(sql`
    SELECT * FROM user_badges
    WHERE user_id = ${userId}
  `);
  return res;
}

async function remove(userId, challengeId, levelId) {
  const res = await db.query(sql`
        DELETE FROM user_badges
        WHERE user_id = ${userId} 
        AND level_id = ${levelId}
        AND challenge_id = ${challengeId}
    `);
  return res;
}

module.exports = {
  add,
  getByUserId,
  remove,
};
