---
title: Introduction
description: Docs intro
layout: ../../layouts/MainLayout.astro
setup: |
    import VideoOverview from '../../components/VideoOverview.astro'
---

**Welcome to the CodeChallenge.dev docs! 🎉 📝**

This documentation is for use alongside [`CodeChallenge.dev`](https://codechallenge.dev). They are written for anyone who wants to use CodeChallenge.dev to participate in challenges or host your own.

As such, there are a few main sections to the documentation, including:

- 🐎 [**Participating in Code Challenges**](/en/joining)
- 🎤 [**Creating and Running Code Challenges**](/en/running)
- 🧑‍💻 [**Contributing to CodeChallenge.dev**](/en/contributing)
- 📝 [**Contributing to the CodeChallenge.dev Documentation**](/en/contributing/docs)
- 👾 [**Self-hosting the CodeChallenge.dev software**](/en/self-hosting)

## Using these Docs

These docs are a work in progress, so while they might not be done yet, you can use the links on the left to navigate around.

## Getting Started

Challenges consist of a few things:

- 1️⃣ A GitLab project used as the starting point for the challenge
- 2️⃣ A set of levels that you can complete using that project or other parts of the challenge

After you join a challenge, typically the first step will be to 🍴 fork the GitLab Project - which you can do right from the challenge page. From there, read the details of the first level and try and complete the challenge.

Your completion status will be automatically updated, and you'll see "Incomplete" go to "Complete" a few minutes after you've completed the challenge.  

## Overview Video

The video below goes over some of the basics of getting started with `CodeChallenge.dev`:

<VideoOverview />

Good luck out there. 🧑‍🚀
