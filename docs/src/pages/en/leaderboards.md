---
title: Leaderboards
description: Code Challenge leaderboards show who has completed various levels of a challenge
layout: ../../layouts/MainLayout.astro
setup: |
    import DocImage from '../../components/DocImage.astro'
---

## Leaderboards
Leaderboards show folks who have completed various levels for a given challenge. To find the leaderboard for the challenge, go to the challenge homepage and click the leaderboard link on the top right. The leaderboard is displayed on a large screen and automatically refreshes at an interval.

There are two ways to view the leaderboard:
- **By Time** shows all level completions grouped, with the most recent completions at the top left of the screen.
- **By Level** groups completions by level and shows the level title and number, with the most challenging levels at the top of the screen.

<DocImage src="/img/leaderboard.png" alt="A challenge leaderboard" />

## Scoring Updates
Automated score updates happen every 5 minutes. Challenge [admins](/en/running/roles#admin) can also trigger a score update from the [Details page](/en/running/details).  In addition, admins can log completion at almost any time for manual levels. For leaderboards, by default, the page will refresh every 30 seconds to display any updates to the score. The page will remember what tab is selected.

You can control the refresh interval and enable or disable refreshing by checking or unchecking the refresh checkbox or selecting a different time for the refresh interval in the top right section of the page. You can also see the last time the leaderboard was refreshed in the same area.

<DocImage src="/img/leaderboard_refresh.png" alt="The leaderboard refresh section of the leaderboard" />


## Learn More

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge) we'd greatly accept your merge requests! 🥰
