---
title: Challenge Settings
description: Challenge Settings
layout: ../../../layouts/MainLayout.astro
---

Information on the Challenge Settings page is coming soon 🙂

## Challenge Settings

## Learn More

## Contribute

If you'd like to [contribute to this page](https://gitlab.com/gitlab-de/codechallenge) we'd greatly accept your merge requests! 🥰
