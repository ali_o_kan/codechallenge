var cron = require("node-cron");
const dayjs = require("dayjs");
const debug = require("debug")("codingchallenge:cron");

const { updateAllProjectInfo } = require("./challenges");
const { checkScores } = require("./scoring");
const cronFreq = process.env.CRON_FREQ || "*/5 * * * *";

const enabled = process.env.ENABLE_CRON ? true : false;

debug(`CRON is ${enabled ? "" : "NOT "}enabled`);
if (!enabled)
  debug(`Scores will NOT update unless you visit /tasks/checkScores`);

if (enabled) {
  debug("CRON jobs initialized at", dayjs().format(), "freq", cronFreq);
  cron.schedule(cronFreq, () => {
    debug("CRON firing at ", dayjs().format(), "freq", cronFreq);
    updateAllProjectInfo();
    checkScores();
  });
}

module.exports = { debug };
