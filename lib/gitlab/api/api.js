const { Gitlab } = require("@gitbeaker/node");

const api = new Gitlab({
  token: process.env.GITLAB_API_TOKEN,
});

module.exports = api;
