const dayjs = require('dayjs');
const { logger } = require("../../logger");
const api = require("./api");

/**
 * https://docs.gitlab.com/ee/api/members.html#valid-access-levels 
 * 
 * No access (0)
 * Minimal access (5) (Introduced in GitLab 13.5.)
 * Guest (10)
 * Reporter (20)
 * Developer (30)
 * Maintainer (40)
 * Owner (50) - Only valid to set for groups
*/
async function addUserToGroup(groupId, userId) {
    api.GroupMembers.add(groupId, userId, 30, {
        expires_at: dayjs().add(60, 'day').format('YYYY-MM-DD')
    })
    .catch(e => {
        logger.error("Error adding user to group");
        console.error(e);
    })
}

module.exports = {
    addUserToGroup,
}