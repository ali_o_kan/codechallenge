const projects = require("./projects");
const merge_requests = require("./merge_requests");
const groups = require("./groups");
const pipelines = require("./pipelines");

module.exports = {
  projects,
  merge_requests,
  groups,
  pipelines,
};
