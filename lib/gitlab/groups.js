const db = require("../../db");
const groups = require("./api/groups");
const { logger } = require("../logger");

async function addUserToGroup(msg, data) {
    logger.info("newEntrant subscripition GitLab handler");
    const { userId, challengeId } = data;
    const userChallenges = await db.challenges.getDetails(userId);
    const thisChallenge = userChallenges.find(x => x.id === parseInt(challengeId));
    const thisGroupId = thisChallenge.group_id ? thisChallenge.group_id : 0
    if (thisGroupId) {
        logger.info(`Adding user ${userId} to GitLab group ${thisGroupId}`);
        const thisUser = await db.users.getById(userId)
        groups.addUserToGroup(thisGroupId, thisUser.gitlab_id);
    }
}

module.exports = { addUserToGroup }