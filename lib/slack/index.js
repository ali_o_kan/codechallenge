const PubSub = require("pubsub-js");
const { IncomingWebhook } = require("@slack/webhook");

const db = require("../../db");
const {
  imageAcccesory,
  textSection,
  fieldsSection,
  textWithLink,
} = require("./blocks");

const notify = {
  newChallenge: (msg, data) => {
    console.log("TODO: Notify slack", msg, data);
  },
  newEntrant: async (msg, data) => {
    const { challengeId, userId } = data;
    const challenge = await db.challenges.getById(challengeId);
    const user = await db.users.getById(userId);
    const webhooks_record = await db.challenge_webhooks.getById(challengeId);

    if (webhooks_record) {
      if (webhooks_record.newentrant) {
        const webhook = new IncomingWebhook(webhooks_record.hook_url, {
          username: webhooks_record.username,
          icon_emoji: webhooks_record.icon_emoji,
          channel: webhooks_record.channel,
        });
        let blocks = [];

        blocks.push(textSection(`*${challenge.name}* has a new entrant!`));
        const fieldS = fieldsSection(
          [
            `*Name:*\n${user.name}`,
            `*GitLab Handle:*\n${user.gitlab_handle}`,
            `*Email:*\n${user.email}`,
          ],
          imageAcccesory(user.picture, user.name)
        );
        blocks.push(fieldS);

        const slackMessage = {
          text: `${user.name} has joined the challenge ${challenge.name}`,
          blocks,
          icon_emoji: ":new:",
        };

        try {
          webhook.send(slackMessage).catch((e) => console.error(e));
          return true;
        } catch (error) {
          console.error(error);
          return false;
        }
      }
    }

    return null;
  },
  levelCompleted: async (msg, data) => {
    const { challengeId, userId, levelId } = data;
    const promises = [
      db.challenges.getById(challengeId),
      db.users.getById(userId),
      db.challenge_webhooks.getById(challengeId),
      db.levels.getById(levelId),
      db.scoring.singleRecord(levelId, challengeId, userId),
    ];
    const promiseData = await Promise.all(promises);
    const challenge = promiseData[0];
    const user = promiseData[1];
    const webhooks_record = promiseData[2];
    const level = promiseData[3];
    const scoring = promiseData[4];

    if (webhooks_record) {
      if (webhooks_record.levelcomplete) {
        const webhook = new IncomingWebhook(webhooks_record.hook_url, {
          username: webhooks_record.username,
          icon_emoji: webhooks_record.icon_emoji,
          channel: webhooks_record.channel,
        });
        let blocks = [];

        blocks.push(
          textSection(`*${challenge.name}* has a new completed level!`)
        );
        blocks.push(
          fieldsSection([
            `*Name:*\n${user.name}`,
            `*Level:*\n${level.level}`,
            `*GitLab Handle:*\n<https://gitlab.com/${user.gitlab_handle}|@${user.gitlab_handle}>`,
            `*Level Name:*\n${level.title}`,
          ])
        );

        if (scoring.ref) {
          blocks.push(
            textSection(`*Reference:*\n<${scoring.ref}|${scoring.ref}>`)
          );
        }

        const slackMessage = {
          text: `${user.name} has completed level ${level.level} of ${challenge.name}`,
          blocks,
          icon_emoji: ":white_check_mark:",
        };

        try {
          webhook.send(slackMessage).catch((e) => console.error(e));
          return true;
        } catch (error) {
          console.error(error);
          return false;
        }
      }
    }
    return null;
  },
  testWebhook: async (msg, data) => {
    const { challengeId } = data;
    const { hook_url } = await db.challenge_webhooks.getById(challengeId);
    const slackWebhook = new IncomingWebhook(webhooks_record.hook_url, {
      username: webhooks_record.username,
      icon_emoji: webhooks_record.icon_emoji,
      channel: webhooks_record.channel,
    });

    const blocks = [];
    blocks.push(textSection("Test webhook from CodeChallenge.dev"));
    blocks.push(
      textWithLink(
        "Check out the leaderboard!",
        `${process.env.BASE_URL}/leaderboard/${challengeId}`
      )
    );

    return slackWebhook.send({
      text: "Test webhook from CodeChallenge.dev",
      blocks,
    });
  },
};

PubSub.subscribe("newChallenge", notify.newChallenge);
PubSub.subscribe("newEntrant", notify.newEntrant);
PubSub.subscribe("levelCompleted", notify.levelCompleted);
PubSub.subscribe("testWebhook", notify.testWebhook);

module.exports = notify;
