const { requiresAuth } = require("express-openid-connect");

const db = require("../../db");
const { logger } = require("../../lib/logger");

module.exports = (router) => {
  router.get("/challenges", requiresAuth(), async (req, res, next) => {
    const challenges = await db.challenges.getAll();
    res.json(challenges);
  });

  router.put(
    "/challenge/:challenge_id/deactivate",
    requiresAuth(),
    async (req, res) => {
      const challengeId = req.params.challenge_id;
      const openIDuser = await req.oidc.fetchUserInfo();
      const thisUser = await db.users.getOrCreateUser(openIDuser);
      userId = thisUser.id;

      if (userId) {
        const userChallenge = await db.challenges.userChallenge(
          challengeId,
          userId
        );
        if (userChallenge.isAdmin) {
          db.challenges
            .changeActive(challengeId, false)
            .then((v) => {
              res.json(v);
            })
            .catch((e) => {
              console.error(e);
              res.status(500).json(e);
            });
        } else {
          res.status(401).send("https://http.cat/401");
        }
      } else {
        res.status(400).send("User is not logged in");
      }
    }
  );
};
