var express = require("express");
var router = express.Router();
const { requiresAuth } = require("express-openid-connect");

const db = require("../../db");
const { logger } = require("../../lib/logger");

const isStandalone = process.env.STANDALONE;
const challenges = require("./challenges");

const public = require("./public")(router);

router.get("/*", requiresAuth(), async (req, res, next) => {
  let { token_type, access_token, isExpired, refresh } = req.oidc.accessToken;
  if (isExpired()) {
    logger.info("Refreshing token");
    ({ access_token } = await refresh());
  }
  const openIDuser = await req.oidc.fetchUserInfo();
  const thisUser = await db.users.getOrCreateUser(openIDuser);
  req.currentUser = thisUser;
  next();
});

router.get("/currentUser", async (req, res, next) => {
  res.send(`You again? ${isStandalone}`);
});

challenges(router);

module.exports = router;
