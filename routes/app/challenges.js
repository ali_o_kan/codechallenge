const { requiresAuth } = require("express-openid-connect");

const db = require("../../db");
const { logger } = require("../../lib/logger");
const { userLevelDetails } = require("../../lib/scoreboard");
const { isEnabled } = require("../../lib/unleash");

module.exports = (router) => {
  router.get("/challenge/new", requiresAuth(), async (req, res) => {
    res.render("app/newChallenge", { currentUser: req.currentUser });
  });

  router.get("/challenge/:id", requiresAuth(), async (req, res) => {
    const challengeId = req.params.id;
    const wholeCardSuccess = isEnabled("whole-card-success");
    const promises = [
      db.challenges.getById(challengeId),                           // challenge
      db.challenges.userChallenge(challengeId, req.currentUserId),  // userChallenge
      db.levels.getAllByChallengeId(challengeId),                   // levels
      db.users_challenges.getEntrants(challengeId),                 // entrants
      db.scoring.getCompleteByChallenge(challengeId),               // scores
    ];
    const [challenge, userChallenge, levels, entrants, scores] =
      await Promise.all(promises).catch((e) => console.error(e));

    for (const level of levels) {
      level.myComplete = scores.find(
        (s) => s.level_id === level.id && s.user_id === req.currentUserId
      );
      level.completedEntrants = scores.filter((v) => v.level_id === level.id);
      for (const entrant of level.completedEntrants) {
        const thisEnt = entrants.find((v) => v.user_id === entrant.user_id);
        entrant.entrant = thisEnt;
      }
      level.tenComplete = level.completedEntrants.slice(0, 10);
    }

    res.render("app/challenge", {
      currentUser: req.currentUser,
      challenge,
      userChallenge,
      levels,
      entrants,
      scores,
      wholeCardSuccess,
    });
  });

  router.get("/challenge/:id/join", requiresAuth(), async (req, res, next) => {
    const didAgree = req.query.agree ? true : false
    db.challenges
      .addUserToChallenge(req.params.id, req.currentUserId, req.query.name, didAgree)
      .then((ret) => {
        res.redirect(`/app/challenge/${req.params.id}?joined=yes`);
      })
      .catch((e) => {
        logger.error("Error with user joining challenge", e);
        if (e.constraint) {
          if (e.constraint == "users_challenges_user_id_challenge_id_key") {
            res.redirect(`/app/challenge/${req.params.id}?joined=yes`);
          } else {
            res.redirect(`/app/challenge/${req.params.id}?generalerror=yes`);
          }
        } else {
          res.redirect(`/app/challenge/${req.params.id}?generalerror=yes`);
        }
      });
  });

  router.get("/challenge/:id/settings", requiresAuth(), async (req, res) => {
    const challengeId = req.params.id;
    const promises = [
      db.challenges.getById(challengeId),
      db.challenges.userChallenge(challengeId, req.currentUserId),
      db.challenge_webhooks.getById(challengeId),
    ];
    const data = await Promise.all(promises).catch((e) => console.error(e));
    if (data[1].isAdmin) {
      res.render("app/challengeSettings", {
        currentUser: req.currentUser,
        challenge: data[0],
        userChallenge: data[1],
        webhooks: data[2],
      });
    } else {
      res.redirect("/app?auth=no");
    }
  });

  router.get("/challenge/:id/details", requiresAuth(), async (req, res) => {
    try {
      const challengeId = req.params.id;
      const promises = [
        db.challenges.getById(challengeId),
        db.challenges.userChallenge(challengeId, req.currentUserId),
        db.users_challenges.getEntrants(challengeId),
        db.levels.getAllByChallengeId(challengeId),
        db.scoring.getCompleteByChallenge(challengeId),
      ];
      Promise.all(promises)
        .then((data) => {
          if (data[1].isAdmin) {
            res.render("app/challengeDetails", {
              currentUser: req.currentUser,
              challenge: data[0],
              userChallenge: data[1],
              entrants: data[2],
              levels: data[3],
              completed: data[4],
              userLevelDetails: userLevelDetails(data[2], data[3], data[4]),
            });
          } else {
            res.redirect("/app?auth=no");
          }
        })
        .catch((err) => console.error(err));
    } catch (error) {
      logger.error(error);
      res.status(500).json(error);
    }
  });

  router.get(
    "/challenge/:challenge_id/level/:level_id",
    requiresAuth(),
    async (req, res) => {
      let level = {};
      const userChallenge = await db.challenges.userChallenge(
        req.params.challenge_id,
        req.currentUserId
      );
      if (req.params.level_id != "new") {
        level = await db.levels.getById(req.params.level_id);
      }

      if (userChallenge.isAdmin) {
        res.render("app/challengeLevel", {
          currentUser: req.currentUser,
          challengeId: req.params.challenge_id,
          levelId: req.params.level_id,
          userChallenge,
          level,
        });
      } else {
        res.redirect("/app?auth=no");
      }
    }
  );
};
