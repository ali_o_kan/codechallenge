const { requiresAuth } = require("express-openid-connect");

const db = require("../../db");
const { logger } = require("../../lib/logger");

module.exports = (router) => {
  router.get("/user", requiresAuth(), function (req, res, next) {
    res.render("users/editprofile", {
      user: req.currentUser,
    });
  });
};
